<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("pembelian_model");
		$this->load->model("supplier_model");
		$this->load->model("barang_model");
	}

	public function index()
	{
		$this->listPembelian();
    }
    
    public function listPembelian()
	{
		$data['data_pembelian'] = $this->pembelian_model->tampilDataPembelian();
        
		$this->load->view('home_pembelian', $data);
    }
    
    public function input()
	{
      
        $data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
        
        
        if (!empty($_REQUEST)) {
            $pembelian_header = $this->pembelian_model;
            $pembelian_header->savePembelianHeader();
            $id_terakhir = array();
            //panggil ID transaksi terakhir
            $id_terakhir = $pembelian_header->idTransaksiTerakhir();
           
			redirect("pembelian/inputDetail/" . $id_terakhir, "refresh");
        }
        
		
		$this->load->view('input_pembelian_header', $data);
    }
    
    public function inputDetail($id_pembelian_header)
	{
        // panggil data barang untuk kebutuhan form input
		 $data['id_header'] = $id_pembelian_header;
         $data['data_barang'] = $this->barang_model->tampilDataBarang();
         $data['data_pembelian_detail'] = $this->pembelian_model->tampilDataPembelianDetail($id_pembelian_header);
        
        if (!empty($_REQUEST)) {
          
            $this->pembelian_model->savePembelianDetail($id_pembelian_header);
            
          
            $kode_barang  = $this->input->post('kode_barang');
            $qty        = $this->input->post('qty');
            $this->barang_model->updateStok($kode_barang, $qty);

			redirect("pembelian/inputDetail/" . $id_pembelian_header, "refresh");
        }
        
		
		$this->load->view('input_pembelian_detail', $data);
	}
	
}
